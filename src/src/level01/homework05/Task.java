package level01.homework05;

/*
JavaRus. Learn once - use anywhere
 */

public class Task {
    public static void main(String[] args) {
        String c = "JavaRush. Learn once - use anywhere";
        System.out.println(c);
        System.out.println(c);
        System.out.println(c);
        System.out.println(c);
        System.out.println(c);
        System.out.println(c);
        System.out.println(c);
        System.out.println(c);
        System.out.println(c);
        System.out.println(c);
        //Правильно
    }
}
